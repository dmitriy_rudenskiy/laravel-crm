<?php
namespace App\Services;

use App\Component\FilterInterval;
use App\Models\Task;

class PrinterService
{
    private $list = [];

    public function report(FilterInterval $filter)
    {
        $tmp = Task::whereBetween(
            'start_work',
            [
                $filter->getStart(),
                $filter->getFinish()
            ]
        )->get();

        if ($tmp->count() < 1) {
            return [];
        }

        $this->list = [];

        foreach ($tmp as $value) {
            $this->addToReport($value);
        }

        return $this->list;
    }

    protected function addToReport(Task $task)
    {
        if (!isset($this->list[$task->printer_id])) {
            $this->list[$task->printer_id] = (object)[
                'name' => $task->worker,
                'sum' => 0,
                'items' => []
            ];
        }

        $this->list[$task->printer_id]->sum += (int)$task->price;
        $this->list[$task->printer_id]->items[] = (object)[
            'client' => $task->order->client,
            'order' => '[' . $task->order->id .'] ' . $task->order->name,
            'price'=> (int)$task->price,
            'start' => $task->start_work
        ];
    }

}