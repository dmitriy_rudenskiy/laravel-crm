<?php
namespace App\Repositories;

use App\Models\Worker;
use Illuminate\Database\Eloquent\Model;

class WorkerRepository
{
    /**
     * @var Model
     */
    private $model;

    public function model()
    {
        if ($this->model === null) {
            $this->model = new Worker();
        }

        return $this->model;
    }

    public function getList()
    {
        return $this->model()->orderBy('name')->get();
    }

    public function getNew()
    {
        return clone $this->model();
    }

    /**
     * @param array $data
     * @return Worker|null
     */
    public function add(array $data)
    {
        return $this->model()->forceCreate($data);
    }


    public function get($id)
    {
        return $this->model()->find($id);
    }
}