<?php
namespace App\Repositories;

use App\Component\FilterOrder;
use App\Models\Order;
use Illuminate\Database\Eloquent\Model;

class OrderRepository
{
    /**
     * @var Model
     */
    private $model;

    public function model()
    {
        if ($this->model === null) {
            $this->model = new Order();
        }

        return $this->model;
    }

    public function getNew()
    {
        return clone $this->model();
    }

    /**
     * @param array $data
     * @return Order|null
     */
    public function add(array $data)
    {
        return $this->model()->forceCreate($data);
    }

    public function getList(FilterOrder $filter)
    {
        if ($filter->isEmpty()) {
            return $this->model()->all();
        }

    }

    public function get($id)
    {
        return $this->model()->find($id);
    }
}