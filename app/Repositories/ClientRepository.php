<?php
namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use App\Models\Client;

class ClientRepository
{
    /**
     * @var Model
     */
    private $model;

    public function model()
    {
        if ($this->model === null) {
            $this->model = new Client();
        }

        return $this->model;
    }

    /**
     * @param array $data
     * @return Client|null
     */
    public function add(array $data)
    {
        return $this->model()->forceCreate($data);
    }

    /**
     * @return Client[]
     */
    public function getList()
    {
        return $this->model()->orderBy('name')->get();
    }

    /**
     * @param int $id
     * @return Client|null
     */
    public function get($id)
    {
        return $this->model()->find($id);
    }

    public function getNew()
    {
        return clone $this->model();
    }
}