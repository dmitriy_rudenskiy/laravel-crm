<?php

namespace App\Component;

class FilterOrder
{
    private $start;

    private $finish;

    private $client;

    private $manager;

    public function __construct($data)
    {
        if (!is_array($data) || sizeof($data) < 1) {
            return;
        }
    }

    public function isEmpty()
    {
        return ($this->start === null && $this->finish === null
            && $this->client === null && $this->manager === null);
    }
}