<?php
namespace App\Component;

use DateTime;
use DateInterval;

class FilterInterval
{
    private $start;

    private $finish;

    const FORMAT = 'd.m.Y';

    public function __construct(array $data)
    {
        if (!empty($data['start'])) {
            $this->start =  DateTime::createFromFormat(self::FORMAT, trim($data['start']));
        }

        if (!empty($data['finish'])) {
            $this->finish =  DateTime::createFromFormat(self::FORMAT, trim($data['finish']));
        }

        if ($this->isEmpty()) {
            $this->start =  new DateTime();
            $this->start->sub(new DateInterval('P7D'));
            $this->finish = new DateTime();
        }
    }

    /**
     * @return mixed
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * @return mixed
     */
    public function getFinish()
    {
        return $this->finish;
    }

    public function isEmpty()
    {
        return ($this->start === null && $this->finish === null);
    }
}