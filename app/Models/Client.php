<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table = 'client';

    public $timestamps = false;
    
    public function __toString()
    {
        return $this->name;
    }
}
