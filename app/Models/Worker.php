<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Worker extends Model
{
    protected $table = 'worker';

    public $timestamps = false;

    public function type()
    {
        return $this->belongsTo('App\Models\WorkerType', 'type_id');
    }

    public function __toString()
    {
        return $this->name;
    }
}
