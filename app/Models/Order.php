<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'order';
    
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function client()
    {
        return $this->belongsTo('App\Models\Client', 'client_id');
    }

    public function task()
    {
        return $this->hasMany('App\Models\Task', 'order_id');
    }
}
