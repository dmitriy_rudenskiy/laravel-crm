<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WorkerType extends Model
{
    protected $table = 'worker_type';

    public $timestamps = false;

    public function __toString()
    {
        return $this->name;
    }
}
