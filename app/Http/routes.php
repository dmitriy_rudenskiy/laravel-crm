<?php
Route::auth();

Route::group(['middleware' => 'auth'], function () {
    Route::get('/', ['as' => 'root', 'uses' => 'HomeController@index'])->name('');
    Route::get('/home', 'HomeController@index');

    // пользователи
    Route::get('/manager',  'UserController@index')->name('user_list');
    Route::get('/user/view/{id}', 'UserController@view')->name('user_view');
    Route::get('/user/create', 'UserController@create')->name('user_create');
    Route::post('/user/update', 'UserController@update')->name('user_update');
    Route::post('/user/create', 'UserController@insert')->name('user_create');

    // клиенты
    Route::get('/client', 'ClientController@index')->name('client');
    Route::get('/client/add', 'ClientController@create')->name('client_add');
    Route::get('/client/view/{id}', 'ClientController@view')->name('client_view');
    Route::post('/client/update', 'ClientController@update')->name('client_update');

    // работа с заказами
    Route::get('/order', 'OrderController@index')->name('order');
    Route::get('/order/create', 'OrderController@create')->name('order_create');
    Route::get('/order/view/{id}', 'OrderController@view')->name('order_view');
    Route::post('/order/insert', 'OrderController@insert')->name('order_insert');

    // Список задачь
    Route::get('/task', 'TaskController@index')->name('task');
    Route::post('/task/insert', 'TaskController@insert')->name('task_insert');
    Route::get('/task/get', 'TaskController@get')->name('task_get');

    // исполнители
    Route::get('/worker', 'WorkerController@index')->name('worker');
    Route::get('/worker/create', 'WorkerController@create')->name('worker_create');
    Route::get('/worker/view/{id}', 'WorkerController@view')->name('worker_view');
    Route::post('/worker/update', 'WorkerController@update')->name('worker_update');

    // отчёты
    Route::get('/report', 'ReportController@index')->name('report');
    Route::get('/report/worker', 'ReportController@printer')->name('report_printer');
});