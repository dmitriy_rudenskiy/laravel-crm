<?php
namespace App\Http\Controllers;

use App\Component\FilterOrder;
use App\Models\Order;
use App\Models\Printer;
use App\Models\Worker;
use App\Repositories\ClientRepository;
use App\Repositories\OrderRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class SiteController
 * @package App\Http\Controllers
 */
class OrderController extends Controller
{
    /**
     * @var Order
     */
    private $model;

    private $client;

    public function __construct()
    {
        $this->model = new OrderRepository();
        $this->client = new ClientRepository();
    }

    public function index(Request $request)
    {
        $filter = new FilterOrder($request->all());

        $list = $this->model->getList($filter);
        $clients = $this->client->getList();

        return view('order.index',
            [
                'list' => $list,
                'clients' =>  $clients
            ]
        );
    }

    public function view($id)
    {
        $order = $this->model->get($id);
        $printer = Worker::all();

        return view('order.view', ['order' => $order, 'workers' => $printer]);
    }

    public function create()
    {
        $clients = $this->client->getList();

        return view('order.create', ['clients' =>  $clients]);
    }

    public function insert(Request $request)
    {
        $data = $request->only(['client_id', 'price', 'amount', 'name', 'description', 'path', 'ready']);
        $data['user_id'] = Auth::user()->id;
        list($day, $month, $year) = explode('.', $data['ready']);
        $data['ready'] = sprintf("%s-%s-%s", $year, $month, $day);


        $this->model->add($data);
        return redirect()->route('order');
    }
}