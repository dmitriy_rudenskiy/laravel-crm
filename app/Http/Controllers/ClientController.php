<?php
namespace App\Http\Controllers;

use App\Repositories\ClientRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ClientController extends Controller
{
    /**
     * @var ClientRepository
     */
    private $model;

    public function __construct()
    {
        $this->model = new ClientRepository();
    }

    public function index()
    {
        $list = $this->model->getList();

        return view('client.index', ['list' => $list]);
    }

    public function create()
    {
        return view('client.view', ['client' => $this->model->getNew()]);
    }

    public function view($id)
    {
        $client = $this->model->get($id);

        return view('client.view', ['client' => $client]);
    }

    public function update(Request $request)
    {
        $id = (int)$request->get('id');
        $name = $request->get('name');

        if (empty($name)) {
            Session::flash('error', -100);
            return redirect()->back();
        }

        if ($id < 1) {
            $this->model->add(['name' => $name]);
        } else {
            $client = $this->model->get($id);
            $client->name = $name;
            $client->save();
        }

        Session::flash('success', 1);
        return redirect()->route('client');
    }
}