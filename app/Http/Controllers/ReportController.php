<?php
namespace App\Http\Controllers;

use App\Component\FilterInterval;
use App\Services\PrinterService;
use Illuminate\Http\Request;

/**
 * Class SiteController
 * @package App\Http\Controllers
 */
class ReportController extends Controller
{
    public function index()
    {
        return view('report.index', []);
    }

    public function printer(Request $request)
    {
        $filter = new FilterInterval($request->only(['start', 'finish']));

        $list = (new PrinterService())->report($filter);

        return view(
            'report.list',
            [
                'list' => $list,
                'filter' => $filter
            ]
        );
    }
}