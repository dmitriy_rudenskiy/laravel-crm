<?php
namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    /**
     * @var User
     */
    private $model;

    public function __construct()
    {
        $this->model = new User();
    }

    /**
     * Список пользователей
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $list = $this->model->all();

        return view('user.index', ['list' => $list]);
    }

    /**
     * Просмотр пользователя
     *
     * @param int $id
     * @return \Illuminate\View\View
     */
    public function view($id)
    {
        $user = $this->model->findOrFail($id);

        return view('user.view', ['user' => $user]);
    }

    public function create()
    {
        return view('user.create');
    }

    public function insert(Request $request)
    {
        $data = $request->only(['name', 'email', 'password']);

        DB::table('user')->insert([
            'name' => trim($data['name']),
            'email' => trim(strtolower($data['email'])),
            'password' => bcrypt(trim($data['password']))
        ]);

        Session::flash('success', 1);

        return redirect()->route('user_list');
    }


    public function update(Request $request)
    {
        $id = (int)$request->get('id');

        $user = $this->model->findOrFail($id);

        $data = $request->only(['email', 'name', 'password']);

        if (empty($data['password'])) {
            unset($data['password']);
        } else {
            $data['password'] = bcrypt(trim($data['password']));
        }

        if (empty($data['email'])) {
            unset($data['email']);
        } else {
            $data['email'] = strtolower(trim($data['email']));
        }

        $user->update($data);

        Session::flash('success', 1);

        return redirect()->route('user_list');
    }


    public function login(Request $request)
    {
        $error = $request->get('error');

        return view('user.login', ['error' => $error]);
    }

    public function check(Request $request)
    {
        $data = $request->only('login', 'password');
        $user = (new UserService())->auth($data['login'], $data['password']);

        if ($user !== null) {
            session(['user' => serialize($user)]);
            return redirect()->route('root');
        }

        return redirect()->route('login', ['error' => 1]);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout()
    {
        session(['user' => null]);
        return redirect()->route('login');
    }
}