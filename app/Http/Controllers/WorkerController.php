<?php
namespace App\Http\Controllers;

use App\Models\Printer;
use App\Models\PrinterType;
use App\Repositories\WorkerRepository;
use App\Repositories\WorkerTypeRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

/**
 * Class SiteController
 * @package App\Http\Controllers
 */
class WorkerController extends Controller
{
    private $model;

    private $typeRepository;

    public function __construct()
    {
        $this->model = new WorkerRepository();

        $this->typeRepository = new WorkerTypeRepository();
    }

    public function index()
    {
        $list = $this->model->getList();

        return view('worker.index', ['list' => $list]);
    }

    public function view($id)
    {
        $worker = $this->model->get($id);
        $types = $this->typeRepository->getList();

        return view('worker.view', ['worker' => $worker, 'types' => $types]);
    }

    public function create()
    {
        $worker = $this->model->getNew();
        $types = $this->typeRepository->getList();

        return view('worker.view', ['worker' => $worker, 'types' => $types]);
    }

    public function update(Request $request)
    {
        $id = $request->get('id');
        $name = $request->get('name');
        $typeId = $request->get('type_id');

        $data = [
            'name' => trim($name),
            'type_id' => (int)$typeId
        ];

        if ($id > 0) {
            $model = $this->model->get($id);
            if ($model !== null) {
                $model->name = trim($name);
                $model->type_id = (int)$typeId;
                $model->save();
            }
        } else {
            $this->model->add($data);
        }


        Session::flash('success', 1);

        return redirect()->route('worker');
    }
}