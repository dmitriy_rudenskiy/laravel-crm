<?php

use Illuminate\Database\Seeder;
use App\Models\WorkerType;

class WorkerTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->getData() as $value) {
            WorkerType::forceCreate($value);
        }
    }

    public function getData()
    {
        return [
            [
                'id' => 1,
                'name' => 'дизайн'
            ],
            [
                'id' => 2,
                'name' => 'ярче'
            ],
            [
                'id' => 3,
                'name' => 'поставщики'
            ],
        ];
    }
}
